function carModels(inventory){
    if ((typeof(inventory) === 'undefined') || (inventory.length === 0) ){
        return []
    };
    if (inventory.length > 0){
        let carModel = [];
    for (let i = 0; i < inventory.length; i++){
        let model = inventory[i].car_model;
        carModel.push(model);
    }

    return carModel;
    }
    
}

module.exports = carModels;