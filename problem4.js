function carYears(inventory) {
    if ((typeof(inventory) === 'undefined') || (inventory.length === 0) ){
        return []
    };
    if (inventory.length > 0){
        let years = [];
    for (let i = 0; i < inventory.length; i++){
        years.push(inventory[i].car_year)
    }
    return years
    }
    
}
module.exports = carYears;