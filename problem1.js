function findId(inventory, carId) {
    if ((typeof(carId) === 'undefined') || (typeof(inventory) === 'undefined') || (inventory.length === 0) ){
           return []
    };
    if (inventory.length > 0 ){
        for (let i = 0; i < inventory.length; i++) {
        
            if (inventory[i].id === carId) {
                return inventory[i];
            }
        
        }
    }
}


module.exports = findId;